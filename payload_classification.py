import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import sys
from keras.preprocessing.sequence import pad_sequences

#sequence_maxlength = sys.argv[1]
sequence_maxlength = 80

charset = ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
            "p","q","r","s","t","u","v","w","x","y","z","`","~","1","!",
            "2","@","3","#","4","$","5","%","6","^","7","&","8","*","9",
            "(","0",")","-","_","=","+","[","{","]","}","\\","|",";",":",
            "\'","\"",",","<",".",">","/","?","※","☆","♡","\b","\t","\n","\c",
            "\r"," "
)

def one_hot_ecoding(str):

    x = []
    for i,char in enumerate(str):
        try:
            num = charset.index(char)
        except:
            print("Unhandeld Character :",char)
            sys.exit()
            
        vector = [0] * len(charset)
        vector[num] = 1
        x.append(vector)
    
    return x


def file_to_hotencoding(path,label_number):

    X = []
    Y = []

    lines = open(path,"r").readlines()
    for line in lines:
        
        
        line = line.lower()
        x = one_hot_ecoding(line)
        X.append(np.asarray(x).astype(np.float32))
        Y.append(label_number)
        

    return X,Y
    
    
    
X,Y = file_to_hotencoding("threat.txt",1)
legX, legY = file_to_hotencoding("legitimate.txt",0)

print(len(X),len(X[0]))
print(len(Y))
print(len(legX),len(legX[0]))
print(len(legY))
X.extend(legX)
Y.extend(legY)
X= pad_sequences(X,dtype='object',maxlen=sequence_maxlength,value= [0]*len(charset),padding="post")
print("========================================================")
print(len(X),len(X[0]))
print(len(Y))

X = np.asarray(X).astype(np.float32)
Y = np.asarray(Y).astype(np.float32)
print("========================================================")
print(X.shape,X[0].shape)
print(Y.shape)



model = keras.Sequential()
model.add(keras.Input((None,77)))
model.add(layers.LSTM(50))
model.add(layers.Dense(1, activation='sigmoid'))

model.compile(optimizer="Adam", loss="mse")

print("========================================================")
history = model.fit(X, Y,epochs=50, batch_size=50, verbose=1)
print("\n\n\n")
while True:
    print("===================================\n")
    testurls = [] 
    testurl= input("Input pay-load : ")
    testurl = testurl.lower()
    print("소문자 변환 :",testurl)
    testurl = one_hot_ecoding(testurl)
    print("input size before hot-encoding : ",len(testurl))
    testurls.append(testurl)
    testurls= pad_sequences(testurls,dtype='object',maxlen=sequence_maxlength,value= [0]*len(charset),padding="post")
    print("input size after encoding : ",len(testurls),len(testurls[0]))
    predictions = model.predict(np.asarray(testurls).astype(np.float32))
    print(predictions)
    if predictions[0] < 0.5:
        print("\n\n결과 : 정상으로 판단. Confidence("+str(0.5-predictions[0])+")")
    else :
        print("\n\n결과 : 공격으로 판단. Confidence("+str(predictions[0]-0.5)+")")
    print("===================================\n\n")
